package databaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnection {
	public static class DB {
		public static Connection getConnection(){
			Connection con=null;
			try{
				Class.forName("com.mysql.jdbc.Driver");
				con=DriverManager.getConnection("jdbc:mysql://localhost:3306/book_store","root","root");
			}catch(Exception e){System.out.println(e);}
			return con;
		}

	}

}
